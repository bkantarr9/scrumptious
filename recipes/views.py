from django.shortcuts import render
from recipes.models import Recipe

# Create your views here.
def show_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request,"recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request,"recipes/list.html", context)
